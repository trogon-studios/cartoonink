﻿using System;

namespace Trogon.CartoonInk.Services
{
    public class SuspensionState
    {
        public object Data { get; set; }

        public DateTime SuspensionDate { get; set; }
    }
}
