﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;

using Trogon.CartoonInk.Models;

using Windows.Storage;
using Windows.Storage.Provider;
using Windows.Storage.Streams;
using Windows.UI.Input.Inking;

namespace Trogon.CartoonInk.Services
{
    public static class GalleryDataService
    {
        const string ImagesDirName = "Images";

        static ObservableCollection<GalleryImage> galleryData;

        public static ObservableCollection<GalleryImage> GetGalleryData()
        {
            return galleryData;
        }

        public async static Task<ObservableCollection<GalleryImage>> GetGalleryDataAsync()
        {
            var imagesDir = await GetImagesFolder();
            var files = await imagesDir.GetFilesAsync();

            var data = new ObservableCollection<GalleryImage>();
            foreach (var imageFile in files)
            {
                data.Add(new GalleryImage()
                {
                    ID = $"{Path.GetFileNameWithoutExtension(imageFile.Path)}",
                    Source = imageFile.Path,
                    Name = $"Image sample {imageFile.DisplayName}"
                });
            }

            galleryData = data;

            return data;
        }

        internal static async Task SaveImageAsync(InkStrokeContainer strokeContainer, string name)
        {
            // Get all strokes on the InkCanvas.
            IReadOnlyList<InkStroke> currentStrokes = strokeContainer.GetStrokes();

            // Strokes present on ink canvas.
            if (currentStrokes.Count > 0)
            {
                string filename = $"{name}.gif";
                StorageFolder folder = await GetImagesFolder();

                StorageFile file = (await folder.TryGetItemAsync(filename)) as StorageFile;
                if (file == null)
                {
                    file = await folder.CreateFileAsync(filename);
                }

                // Prevent updates to the file until updates are finalized with call to CompleteUpdatesAsync.
                CachedFileManager.DeferUpdates(file);

                // Open a file stream for writing.
                IRandomAccessStream stream = await file.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite);

                // Write the ink strokes to the output stream.
                using (IOutputStream outputStream = stream.GetOutputStreamAt(0))
                {
                    await strokeContainer.SaveAsync(outputStream);
                    await outputStream.FlushAsync();
                }

                stream.Dispose();

                // Finalize write so other apps can update file.
                FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);

                if (status == FileUpdateStatus.Complete)
                {
                    // File saved.
                }
            }
        }

        private async static Task<StorageFolder> GetImagesFolder()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            var imagesDir = await storageFolder.TryGetItemAsync(ImagesDirName) as StorageFolder;
            if (imagesDir == null)
            {
                imagesDir = await storageFolder.CreateFolderAsync(ImagesDirName);
            }
            return imagesDir;
        }
    }
}

