﻿using System;
using System.Threading.Tasks;

using Trogon.CartoonInk.Helpers;
using Trogon.CartoonInk.Models;

using Windows.Storage;
using Windows.UI.Xaml;

namespace Trogon.CartoonInk.Services
{
    public static class DrawCanvasService
    {
        private const string DrawInputDeviceSettingsKey = "AppDrawInputDevice";

        public static InputDevices DrawInputDeviceType { get; set; } = InputDevices.Mouse | InputDevices.Pen | InputDevices.Touch;

        public static async Task InitializeAsync()
        {
            DrawInputDeviceType = await LoadThemeFromSettingsAsync();
        }

        private static async Task<InputDevices> LoadThemeFromSettingsAsync()
        {
            InputDevices cacheInputDevice = InputDevices.Mouse | InputDevices.Pen | InputDevices.Touch;
            string inputDeviceName = await ApplicationData.Current.LocalSettings.ReadAsync<string>(DrawInputDeviceSettingsKey);

            if (!string.IsNullOrEmpty(inputDeviceName))
            {
                Enum.TryParse(inputDeviceName, out cacheInputDevice);
            }

            return cacheInputDevice;
        }

        public static async Task SetDrawInputDeviceAsync(InputDevices inputDevices)
        {
            DrawInputDeviceType = inputDevices;
            await SaveDrawInputDeviceInSettingsAsync(inputDevices);
        }

        private static async Task SaveDrawInputDeviceInSettingsAsync(InputDevices inputDevices)
        {
            await ApplicationData.Current.LocalSettings.SaveAsync(DrawInputDeviceSettingsKey, inputDevices.ToString());
        }
    }
}
