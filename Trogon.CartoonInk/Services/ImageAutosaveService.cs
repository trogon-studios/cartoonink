﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Windows.UI.Input.Inking;
using Windows.UI.Xaml;

namespace Trogon.CartoonInk.Services
{
    public class ImageAutosaveService
    {
        static DispatcherTimer autosaveTimer;
        static bool isCurrentlySaving;
        static InkPresenter imageInkPresenter;
        static string imageName;

        static ImageAutosaveService()
        {
            autosaveTimer = new DispatcherTimer();
            autosaveTimer.Tick += (s, e) => OnAutosave();

        }

        public static void DrawingStarted()
        {
            ResetAutosaveTimer();
        }

        public static void DrawingFinished(InkPresenter inkPresenter, string name)
        {
            imageInkPresenter = inkPresenter;
            imageName = name;
            StartAutosaveTimer(100);
        }

        public static void InkCleared(InkPresenter inkPresenter, string name)
        {
            imageInkPresenter = inkPresenter;
            imageName = name;
            StartAutosaveTimer();
        }

        private static async void OnAutosave()
        {
            ResetAutosaveTimer();
            await SaveAsync();
        }

        private static void StartAutosaveTimer(int delayInMilliseconds = 500)
        {
            ResetAutosaveTimer();
            autosaveTimer.Interval = new TimeSpan(delayInMilliseconds * 10 * 1000);
            autosaveTimer.Start();
        }

        private static void ResetAutosaveTimer()
        {
            autosaveTimer.Stop();
        }

        public static async Task SaveAsync()
        {
            // Cannot await in lock statement
            if (!isCurrentlySaving)
            {
                isCurrentlySaving = true;
                await GalleryDataService.SaveImageAsync(imageInkPresenter.StrokeContainer, imageName);
                isCurrentlySaving = false;
            }
        }
    }
}
