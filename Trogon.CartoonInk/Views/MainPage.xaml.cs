﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Trogon.CartoonInk.Services;
using Trogon.CartoonInk.ViewModels;

using Windows.UI.Core;
using Windows.UI.Input.Inking;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Trogon.CartoonInk.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel { get; } = new MainViewModel();

        public MainPage()
        {
            InitializeComponent();

            ViewModel.InitInkPresenter(inkCanvas.InkPresenter);
            ViewModel.InitInkToolbar(inkToolbar);
        }
    }
}
