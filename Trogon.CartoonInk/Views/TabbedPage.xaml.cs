﻿using System;

using Trogon.CartoonInk.ViewModels;

using Windows.UI.Xaml.Controls;

namespace Trogon.CartoonInk.Views
{
    public sealed partial class TabbedPage : Page
    {
        public TabbedViewModel ViewModel { get; } = new TabbedViewModel();

        public TabbedPage()
        {
            InitializeComponent();
        }
    }
}
