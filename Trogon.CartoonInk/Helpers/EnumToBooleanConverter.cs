﻿using System;
using System.Reflection;

using Windows.UI.Xaml.Data;

namespace Trogon.CartoonInk.Helpers
{
    public class EnumToBooleanConverter : IValueConverter
    {
        public Type EnumType { get; set; }

        public string EnumTypeString { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (EnumTypeString != null && EnumType == null)
            {
                EnumType = GetType().Assembly.GetType(EnumTypeString);
            }

            if (parameter is string enumString)
            {
                if (EnumType.GetCustomAttribute<FlagsAttribute>() != null)
                {
                    if (EnumType != value?.GetType())
                    {
                        throw new ArgumentException("ExceptionEnumToBooleanConverterValueMustBeAnEnum".GetLocalized());
                    }

                    var enumValue = (Enum)Enum.Parse(EnumType, enumString);

                    return ((Enum)value).HasFlag(enumValue);
                }
                else
                {
                    if (!Enum.IsDefined(EnumType, value))
                    {
                        throw new ArgumentException("ExceptionEnumToBooleanConverterValueMustBeAnEnum".GetLocalized());
                    }

                    var enumValue = Enum.Parse(EnumType, enumString);

                    return enumValue.Equals(value);
                }
            }

            throw new ArgumentException("ExceptionEnumToBooleanConverterParameterMustBeAnEnumName".GetLocalized());
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (EnumTypeString != null && EnumType == null)
            {
                EnumType = this.GetType().Assembly.GetType(EnumTypeString);
            }

            if (parameter is string enumString)
            {
                return Enum.Parse(EnumType, enumString);
            }

            throw new ArgumentException("ExceptionEnumToBooleanConverterParameterMustBeAnEnumName".GetLocalized());
        }
    }
}
