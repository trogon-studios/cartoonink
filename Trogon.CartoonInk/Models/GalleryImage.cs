﻿namespace Trogon.CartoonInk.Models
{
    public class GalleryImage
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Source { get; set; }
    }
}
