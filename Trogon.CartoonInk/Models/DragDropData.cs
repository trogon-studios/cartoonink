﻿using Windows.ApplicationModel.DataTransfer;

namespace Trogon.CartoonInk.Models
{
    public class DragDropData
    {
        public DataPackageOperation AcceptedOperation { get; set; }

        public DataPackageView DataView { get; set; }
    }
}
