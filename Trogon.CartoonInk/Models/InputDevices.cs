﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trogon.CartoonInk.Models
{
    [Flags]
    public enum InputDevices
    {
        //
        // Summary:
        //     No input.
        None = 0,
        //
        // Summary:
        //     Expose touch input events.
        Touch = 1,
        //
        // Summary:
        //     Expose pen input events.
        Pen = 2,
        //
        // Summary:
        //     Expose mouse input events.
        Mouse = 4
    }
}
