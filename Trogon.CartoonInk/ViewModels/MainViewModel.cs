﻿using System;

using Trogon.CartoonInk.Helpers;
using Trogon.CartoonInk.Services;

using Windows.UI.Core;
using Windows.UI.Input.Inking;
using Windows.UI.Xaml.Controls;

namespace Trogon.CartoonInk.ViewModels
{
    public class MainViewModel : Observable
    {
        private InkPresenter inkPresenter;
        private InkToolbar inkToolbar;
        private string imageName;

        public MainViewModel()
        {
            imageName = $"{DateTime.Now.Ticks}";
        }

        internal void InitInkToolbar(InkToolbar inkToolbar)
        {
            this.inkToolbar = inkToolbar;
            inkToolbar.Loaded += InkToolbar_Loaded;
        }

        private void InkToolbar_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            // Clear all built-in buttons from the InkToolbar.
            inkToolbar.InitialControls = InkToolbarInitialControls.None;

            // Add only the ballpoint pen, pencil, and eraser.
            // Note that the buttons are added to the toolbar in the order
            // defined by the framework, not the order we specify here.
            InkToolbarBallpointPenButton ballpoint = new InkToolbarBallpointPenButton();
            InkToolbarPencilButton pencil = new InkToolbarPencilButton();
            InkToolbarEraserButton eraser = new InkToolbarEraserButton();
            inkToolbar.Children.Add(eraser);
            inkToolbar.Children.Add(ballpoint);
            inkToolbar.Children.Add(pencil);
        }

        internal void InitInkPresenter(InkPresenter inkPresenter)
        {
            this.inkPresenter = inkPresenter;

            // Set supported inking device types.
            inkPresenter.InputDeviceTypes = (CoreInputDeviceTypes)DrawCanvasService.DrawInputDeviceType;

            // Set initial ink stroke attributes.
            InkDrawingAttributes drawingAttributes = new InkDrawingAttributes();
            drawingAttributes.Color = Windows.UI.Colors.Black;
            drawingAttributes.IgnorePressure = false;
            drawingAttributes.FitToCurve = true;
            inkPresenter.UpdateDefaultDrawingAttributes(drawingAttributes);

            inkPresenter.StrokesCollected += InkPresenter_StrokesCollected;
            inkPresenter.StrokesErased += InkPresenter_StrokesErased;
        }

        private void InkPresenter_StrokesCollected(InkPresenter sender, InkStrokesCollectedEventArgs args)
        {
            ImageAutosaveService.DrawingFinished(inkPresenter, imageName);
        }

        private void InkPresenter_StrokesErased(InkPresenter sender, InkStrokesErasedEventArgs args)
        {
            ImageAutosaveService.DrawingFinished(inkPresenter, imageName);
        }
    }
}
