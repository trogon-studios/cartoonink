﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

using Trogon.CartoonInk.Helpers;
using Trogon.CartoonInk.Models;
using Trogon.CartoonInk.Services;

using Windows.ApplicationModel;
using Windows.UI.Xaml;

namespace Trogon.CartoonInk.ViewModels
{
    public class SettingsViewModel : Observable
    {
        private ElementTheme _elementTheme;
        private InputDevices _drawInputDeviceType;

        public ElementTheme ElementTheme {
            get { return _elementTheme; }
            set { Set(ref _elementTheme, value); }
        }

        public InputDevices DrawInputDeviceType {
            get { return _drawInputDeviceType; }
            set { Set(ref _drawInputDeviceType, value); }
        }

        private string _versionDescription;

        public string VersionDescription {
            get { return _versionDescription; }
            set { Set(ref _versionDescription, value); }
        }

        private ICommand _switchThemeCommand;

        public ICommand SwitchThemeCommand {
            get {
                if (_switchThemeCommand == null)
                {
                    _switchThemeCommand = new RelayCommand<ElementTheme>(
                        async (param) =>
                        {
                            ElementTheme = param;
                            await ThemeSelectorService.SetThemeAsync(param);
                        });
                }

                return _switchThemeCommand;
            }
        }

        private ICommand _switchDrawInputDeviceTypeCommand;

        public ICommand SwitchDrawInputDeviceTypeCommand {
            get {
                if (_switchDrawInputDeviceTypeCommand == null)
                {
                    _switchDrawInputDeviceTypeCommand = new RelayCommand<InputDevices>(
                        async (param) =>
                        {
                            var currentDevice = DrawInputDeviceType;

                            if (currentDevice.HasFlag(param))
                            {
                                currentDevice ^= param;
                            }
                            else
                            {
                                currentDevice |= param;
                            }

                            DrawInputDeviceType = currentDevice;
                            await DrawCanvasService.SetDrawInputDeviceAsync(currentDevice);
                        });
                }

                return _switchDrawInputDeviceTypeCommand;
            }
        }

        public SettingsViewModel()
        {
        }

        public void Initialize()
        {
            VersionDescription = GetVersionDescription();
        }

        private bool _hasInstanceBeenInitialized = false;

        public async Task EnsureInstanceInitialized()
        {
            if (!_hasInstanceBeenInitialized)
            {
                _elementTheme = ThemeSelectorService.Theme;
                _drawInputDeviceType = DrawCanvasService.DrawInputDeviceType;

                Initialize();

                _hasInstanceBeenInitialized = true;
            }
            await Task.CompletedTask;
        }

        private string GetVersionDescription()
        {
            var appName = "AppDisplayName".GetLocalized();
            var package = Package.Current;
            var packageId = package.Id;
            var version = packageId.Version;

            return $"{appName} - {version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
        }
    }
}
