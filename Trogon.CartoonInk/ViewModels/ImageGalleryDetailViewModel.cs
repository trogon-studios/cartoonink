﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

using Trogon.CartoonInk.Helpers;
using Trogon.CartoonInk.Models;
using Trogon.CartoonInk.Services;

using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace Trogon.CartoonInk.ViewModels
{
    public class ImageGalleryDetailViewModel : Observable
    {
        private static UIElement _image;
        private object _selectedImage;
        private ObservableCollection<GalleryImage> _source;

        public object SelectedImage {
            get => _selectedImage;
            set {
                if (value is GalleryImage)
                {
                    Set(ref _selectedImage, value);
                    ApplicationData.Current.LocalSettings.SaveString(ImageGalleryViewModel.ImageGallerySelectedIdKey, ((GalleryImage)SelectedImage).ID);
                }
                else
                {
                    Set(ref _selectedImage, _selectedImage);
                }
            }
        }

        public ObservableCollection<GalleryImage> Source {
            get => _source;
            set => Set(ref _source, value);
        }

        public ImageGalleryDetailViewModel()
        {
            Source = GalleryDataService.GetGalleryData();
        }

        public void SetImage(UIElement image) => _image = image;

        public async Task InitializeAsync(string sampleImageId, NavigationMode navigationMode)
        {
            if (!string.IsNullOrEmpty(sampleImageId) && navigationMode == NavigationMode.New)
            {
                SelectedImage = Source.FirstOrDefault(i => i.ID == sampleImageId);
            }
            else
            {
                var selectedImageId = ApplicationData.Current.LocalSettings.ReadString(ImageGalleryViewModel.ImageGallerySelectedIdKey);
                if (!string.IsNullOrEmpty(selectedImageId))
                {
                    SelectedImage = Source.FirstOrDefault(i => i.ID == selectedImageId);
                }
            }

            var animation = ConnectedAnimationService.GetForCurrentView().GetAnimation(ImageGalleryViewModel.ImageGalleryAnimationOpen);
            animation?.TryStart(_image);
            await Task.CompletedTask;
        }

        public void SetAnimation()
        {
            ConnectedAnimationService.GetForCurrentView()?.PrepareToAnimate(ImageGalleryViewModel.ImageGalleryAnimationClose, _image);
        }
    }
}
